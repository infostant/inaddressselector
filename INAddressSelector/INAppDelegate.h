//
//  INAppDelegate.h
//  INAddressSelector
//
//  Created by Nattawut Singhchai on 1/30/14.
//  Copyright (c) 2014 Infostant. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface INAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
