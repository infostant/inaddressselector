//
//  INAddressSelector.m
//  INAddressSelector
//
//  Created by Nattawut Singhchai on 1/30/14.
//  Copyright (c) 2014 Infostant. All rights reserved.
//

#import "INAddressSelector.h"
#import <INSQLiteHelper/INSQLiteHelper.h>

@interface INAddressSelector (){
    INSQLiteHelper *sqlite;
}

@end

@implementation INAddressSelector

-(id)init
{
    if (self = [super init]) {
        sqlite = [[INSQLiteHelper alloc] initWithContentOfFile:[[NSBundle mainBundle] pathForResource:@"THAIADDR" ofType:@"sqlite"]];
    }
    return self;
}

-(NSMutableArray *)query:(NSString *)queryString error:(NSError **)error
{
    return  [sqlite query:queryString error:error];
}

-(NSMutableArray *)provinceListWithError:(NSError **)error
{
    return [self query:@"SELECT * FROM province ORDER BY name ASC" error:error];
}

-(NSMutableArray *)districtListWithProID:(NSNumber *)proid error:(NSError **)error
{
    return [self query:[NSString stringWithFormat:@"SELECT * FROM district WHERE pro_id=%@ ORDER BY name ASC",proid] error:error];
}

-(NSMutableArray *)subdistrictListWithDisID:(NSNumber *)disID proID:(NSNumber *)proid error:(NSError **)error
{
    return [self query:[NSString stringWithFormat:@"SELECT * FROM `sub-district` WHERE pro_id=%@ AND dist_id=%@ ORDER BY name ASC",proid,disID] error:error];
}

-(NSString *)provinceWithProID:(NSNumber *)proid error:(NSError **)error
{
    if (!proid) {
        proid = @0;
    }
    NSString *queryString =[NSString stringWithFormat:@"SELECT name FROM province WHERE id=%@",proid];
    NSMutableArray *query = [self query:queryString error:error];
    if ([query count]) {
        return query[0][@"name"];
    }
    return nil;
}

-(NSString *)districtWithDisID:(NSNumber *)distid error:(NSError **)error
{
    if (!distid) {
        distid = @0;
    }
    NSMutableArray *query = [self query:[NSString stringWithFormat:@"SELECT name FROM district WHERE id=%@",distid] error:error];
    if ([query count]) {
        return query[0][@"name"];
    }
    return nil;
}

-(NSString *)subDistrictWithSubDistID:(NSNumber *)subdistID error:(NSError **)error
{
    if (!subdistID) {
        subdistID = @0;
    }
    NSMutableArray *query = [self query:[NSString stringWithFormat:@"SELECT name FROM `sub-district` WHERE id=%@",subdistID] error:error];
    if ([query count]) {
        return query[0][@"name"];
    }
    return nil;
}


@end
