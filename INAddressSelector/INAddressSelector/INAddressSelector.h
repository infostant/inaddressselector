//
//  INAddressSelector.h
//  INAddressSelector
//
//  Created by Nattawut Singhchai on 1/30/14.
//  Copyright (c) 2014 Infostant. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface INAddressSelector : NSObject

-(NSMutableArray *)provinceListWithError:(NSError **)error;
-(NSString *)provinceWithProID:(NSNumber *)proid error:(NSError **)error;
-(NSMutableArray *)districtListWithProID:(NSNumber *)proid error:(NSError **)error;
-(NSString *)districtWithDisID:(NSNumber *)distid error:(NSError **)error;
-(NSMutableArray *)subdistrictListWithDisID:(NSNumber *)disID proID:(NSNumber *)proid error:(NSError **)error;
-(NSString *)subDistrictWithSubDistID:(NSNumber *)subdistID error:(NSError **)error;

@end
