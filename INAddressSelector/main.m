//
//  main.m
//  INAddressSelector
//
//  Created by Nattawut Singhchai on 1/30/14.
//  Copyright (c) 2014 Infostant. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "INAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([INAppDelegate class]));
    }
}
